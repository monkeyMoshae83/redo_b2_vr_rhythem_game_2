using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockColor
{
    Green,
    Red
}

public class Block : MonoBehaviour
{
    
    public BlockColor color;
    public GameObject brokenBlockLeft;
    public GameObject brokenBlockRight;
    public float brokenBlockForce;
    public float brokenBlockTorque;
    public float brokenBlockDestroyDelay;
    
    void OnTriggerEnter (Collider other)
   {
    if(other.CompareTag("RedSword(Left)"))
    {
        //RedBlock
        if(color == BlockColor.Red && GameManager.instance.leftSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
        {
            //Add score
            GameManager.instance.AddScore();
        }
        else
        {
            //Subtract score
            GameManager.instance.HitWrongBlock();
        }
        
        Hit();
    }
    //Green Block
    else if(other.CompareTag("GreenSword(Green)"))
    {
        if(color == BlockColor.Green && GameManager.instance.rightSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
        {
            //add score
            GameManager.instance.AddScore();
        }
        else
        {
            //subract score
            GameManager.instance.HitWrongBlock();
        }
        
        Hit();
    }
   
   }

    public void Hit ()
    {
        //enable the broken piece
        brokenBlockLeft.SetActive(true);
        brokenBlockRight.SetActive(true);
    
        //remove them as childern
        brokenBlockLeft.transform.parent = null;
        brokenBlockRight.transform.parent = null;
    
        //add force to them
        Rigidbody leftRig = brokenBlockLeft.GetComponent<Rigidbody>();
        Rigidbody rightRig = brokenBlockRight.GetComponent<Rigidbody>();
    
        leftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
        rightRig.AddForce(transform.right * brokenBlockForce, ForceMode.Impulse);
    
        //add Torque
        leftRig.AddTorque(-transform.forward * brokenBlockTorque, ForceMode.Impulse);
        rightRig.AddTorque(transform.forward * brokenBlockTorque, ForceMode.Impulse);
    
        // destory the broken piece after a few second
        Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
        Destroy(brokenBlockRight, brokenBlockDestroyDelay);
    
        //Destory the main block
        Destroy(gameObject);
    }
}
